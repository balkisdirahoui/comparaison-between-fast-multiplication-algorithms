/* In the following we try to implement a set of
 * aritmetic operations that can be used on a finite ring Z/nZ
 * We start by implementing the 3 basic aritmetic operation on integers which are
 * Addition
 * Substraction
 * Division
 * We then implement those same operations on polynomials in a finite ring Z/nZ
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <string.h>
#define DATA_SIZE 200000
#define DEGREE_SIZE 200000
struct poly{
	uint32_t degree;
	int32_t degrees[DEGREE_SIZE];
};
typedef struct poly poly;
// aritmetic operations for integers


uint32_t add(uint32_t a,uint32_t b,uint32_t base){
	// usage of uint_64 in case a+b doesn't fit in 32 bits
	uint64_t temp = a + b;
	// cast the result into a 32 bit int
	uint32_t res = temp % base;
	return res;
}


// for more detail see code for addition
uint32_t mult(int32_t a,int32_t b,int32_t base){
	uint64_t temp = a * b;
	uint32_t res = temp % base;
	return res;
}


uint32_t sub(uint32_t a,uint32_t b,uint32_t base){
	// usage of signed int in this case because b can be bigger than a
	int32_t temp = a - b;
	uint32_t res = temp % base;
	return res;
}

//helper function that puts all the coefs of a polynomial to zero
void initialize_to_zero(poly *a, uint32_t deg){
	a->degree = deg;
	for(int i = 0; i<= a->degree; i++){
		a->degrees[i] = 0;
	}
}


poly* add_poly(poly *a,poly *b){
	poly *result =malloc(sizeof(poly));
	uint32_t deg =  a->degree * (a->degree >= b->degree) + b->degree * (b->degree > a->degree);
	initialize_to_zero(result,deg);
	for (int i = 0; i <= result->degree;i++){
		result->degrees[i] = a->degrees[i] + b->degrees[i];
	}
	return result;
}


poly* sub_poly(poly *a,poly *b){
	poly *result =malloc(sizeof(poly));
	uint32_t deg =  a->degree * (a->degree >= b->degree) + b->degree * (b->degree > a->degree);
	initialize_to_zero(result,deg);
		for (int i = 0; i <= result->degree;i++){
		result->degrees[i] = a->degrees[i] - b->degrees[i];
	}
	return result;
}



void mod_poly(poly* p, int m){
	for (int i = 0; i <= p->degree;i++){
		p->degrees[i] %=m;
	}
}

//Naive Multip
poly* naive(poly *a, poly* b){
	poly *result =malloc(sizeof(poly));
	uint32_t deg =  a->degree  + b->degree ;
	initialize_to_zero(result,deg);

	for (int i = 0; i <= a->degree;i++){
		for (int j=0; j<=b->degree; j++){
			result->degrees[i+j] += a->degrees[i]*b->degrees[j];
   		}
	}
	return result;
}

poly* naive_mod(poly* p,poly* q, int mod){
	poly* r = naive( p, q);
	mod_poly(r,mod);
	return r;
}


//Karatsuba
poly* karatsuba(int n,poly* p,poly* q){
	if(n==0){
		return naive(p,q);
	}

	else{
		int mid;
		if((n%2)==0){
			mid=n/2;
		}
    else {
			mid = (n-1)/2;
		}
		poly *p1, *p0, *q1,*q0,*r0,*r1,*r2,*r2f,*t0,*t1, *t2;
		p1 = malloc(sizeof(poly));
		p0 = malloc(sizeof(poly));
		q0 = malloc(sizeof(poly));
		q1 = malloc(sizeof(poly));
		//Left part of the vector
		initialize_to_zero(p0, mid);
		initialize_to_zero(q0, mid);
		for(int i = 0; i <= mid ; i++){
			p0->degrees[i] = p->degrees[i]; //Coefs (1,4) in example
			q0->degrees[i] = q->degrees[i]; //Coefs (1,3) in example
		}
	  initialize_to_zero(p1, mid);
		initialize_to_zero(q1, mid);
		for(int i = mid+1; i<=n ; i++){
			p1->degrees[i-(mid+1)] = p->degrees[i];
			q1->degrees[i-(mid+1)] = q->degrees[i];
		}
		//karatsuba(1,(1,4),(3,1))
		r0 = karatsuba(mid,p0,q0);
		//r0 = (3,13,4)
		r0->degree = p0->degree+q0->degree;
		//karatsuba(1,(3,1),(2,1))

		r1 = karatsuba(mid,p1,q1);
		r1->degree = p1->degree + q1->degree;
		//t0=(4,5) , t1 = (5,2)
		t0 = add_poly(p0,p1);
		t1 = add_poly(q0,q1);
		//karatsuba(1,(4,5),(5,2))
		r2 = karatsuba(mid,t0,t1);
		r2->degree=t0->degree + t1->degree;

		//R0 = (3,13,4)  R1 = ( 6,5,1) R2=(20,33,10)
		t2 = add_poly(r1,r0);
		r2f= sub_poly(r2,t2);
		//R2 = (11,15,5)

		poly* result;
		result = malloc(sizeof(poly));
		initialize_to_zero(result, p->degree+q->degree);

		for(int i = 0; i<= r0->degree; i++){
			result->degrees[i] += r0->degrees[i];
		}
		for(int i=0; i<=r2->degree;i++){
			result->degrees[i+(mid+1)] += r2f->degrees[i];
		}
		for(int i= 0; i<=r1->degree;i++){
			result->degrees[i+2*(mid+1)] += r1->degrees[i];
		}
		free(p1);free(p0); free(q0); free(q1); free(r0); free(r1); free(r2);free(r2f); free(t0); free(t1);free(t2);
		return result;
	}
}

poly* karatsuba_mod(int n,poly* p,poly* q, int mod){
	poly* r = karatsuba(n, p, q);
	mod_poly(r,mod);
	return r;
}

void add_poly_pointer(poly *a,poly *b, poly* r){
	// select the max degree of a and b and affect it to the result
	// calculate the coefs using the add function to insure that we stay inside the Z/nZ ring
	for (int i = 0; i <= r->degree;i++){
		r->degrees[i] = a->degrees[i] + b->degrees[i];
	}
}

poly* kmult_poly(poly* a, int k){
	poly *result =malloc(sizeof(poly));
	result->degree = a->degree;
	for(int i= 0; i <= result->degree; i++){
		result->degrees[i] = k* a->degrees[i];
	}
	return result;
}

poly* kdiv_poly(poly* a, int k){
	poly *result =malloc(sizeof(poly));
	result->degree = a->degree;
	for(int i= 0; i <= result->degree; i++){
		result->degrees[i] =  a->degrees[i]/k;
	}
	return result;
}

// function that performs toomcook3 multiplication
poly* toom_cook3(poly* p,poly* q){
	// both polynomials are of degree less than n
	int n  = (p->degree * (p->degree >= q->degree) + q->degree * (p->degree < q->degree)) +1;
	int threshold = 4;
	if((n-1) < threshold){
		return karatsuba(n-1 , p, q);
	}
	else{
		// Split the polynomials P and Q into 3 new polynomials each
		int k;
		if((n%3)==0){
			k=n/3;
		}
    else {
			k = n/3 +1;
		}
		poly *r,*p0, *p1, *p2, *q0, *q1, *q2,*p02,*q02, *v0, *v1, *v_1, *v2, *v_inf, *t1, *t2;
		r = malloc(sizeof(poly));
		initialize_to_zero(r,p->degree+ q->degree);
		p0 = malloc(sizeof(poly));
		initialize_to_zero(p0,k-1);
		p1 = malloc(sizeof(poly));
		initialize_to_zero(p1,k-1);
		p2 = malloc(sizeof(poly));
		initialize_to_zero(p2,k-1);
		q0 = malloc(sizeof(poly));
		initialize_to_zero(q0,k-1);
		q1 = malloc(sizeof(poly));
		initialize_to_zero(q1,k-1);
		q2 = malloc(sizeof(poly));
		initialize_to_zero(q2,k-1);
		p02 = malloc(sizeof(poly));
		initialize_to_zero(p02,k-1);
		q02 = malloc(sizeof(poly));
		initialize_to_zero(q02,k-1);
		for(int i=0 ; i < k; i++){
			p0->degrees[i] = p->degrees[i];
			p1->degrees[i] = p->degrees[i+k];
			p2->degrees[i] = p->degrees[i+2*k];
			q0->degrees[i] = q->degrees[i];
			q1->degrees[i] = q->degrees[i+k];
			q2->degrees[i] = q->degrees[i+2*k];
		}
		add_poly_pointer(p0,p2, p02);
		add_poly_pointer(q0,q2, q02);
		v0 = toom_cook3(p0,q0);
		poly *temp1 ,*temp2, *temp3, *temp4, *temp5, *temp6,*temp7, *temp8;
		temp1 = add_poly(p02,p1);
		temp2 = add_poly(q02,q1);
		v1 = toom_cook3(temp1, temp2);
		free(temp1);
		free(temp2);
		temp1 = sub_poly(p02,p1);
		temp2 = sub_poly(q02,q1);
		v_1 = toom_cook3(temp1, temp2);
		free(temp1);free(temp2);
		temp1 = kmult_poly(p2,4);
		temp2 = kmult_poly(p1, 2);
		temp3 = add_poly(p0, temp2);
		temp4 = add_poly(temp3,temp1);
		temp5 = kmult_poly(q2,4);
		temp6 = kmult_poly(q1, 2);
		temp7 = add_poly(q0, temp6);
		temp8 = add_poly(temp7,temp5);
		v2 = toom_cook3(temp4, temp8);
		free(temp1);free(temp2);free(temp3);free(temp4);free(temp5);free(temp6);free(temp7);free(temp8);
		v_inf = toom_cook3(p2,q2);
		temp1 = kmult_poly(v_inf, 2);
		temp2 = kmult_poly(v_1,2);
		temp3 = kmult_poly(v0,3);
		temp4 = add_poly(temp3, temp2);
		temp5 = add_poly(v2,temp4);
		temp6 = kdiv_poly(temp5,6);
		t1 = sub_poly(temp6,temp1);
		free(temp1);free(temp2);free(temp3);free(temp4);free(temp5);free(temp6);
		temp1 = add_poly(v1, v_1);
		t2 = kdiv_poly(temp1,2);
		poly *c1, *c2, *c3;
		c1 = sub_poly(v1,t1);
		temp2 = add_poly(v0,v_inf);
		c2 = sub_poly(t2, temp2);
		c3 = sub_poly(t1, t2);
		for(int i=0 ; i <= k+1; i++){
			r->degrees[i] += v0->degrees[i];
			r->degrees[i+k] += c1->degrees[i];
			r->degrees[i+2*k] += c2->degrees[i];
			r->degrees[i+3*k] += c3->degrees[i];
			r->degrees[i+4*k] += v_inf->degrees[i];
		}
		free(temp1); free(temp2);
		free(c1); free(c2); free(c3);
		free(t1); free(t2);
		free(v0);free(v1); free(v2); free(v_1); free(v_inf);
		free(p0);free(p1); free(p2);free(p02);
		free(q0);free(q1); free(q2);free(q02);
		return r;
	}
}

poly* toom_cook3_mod(poly* p,poly* q, int mod){
	poly* r = toom_cook3( p, q);
	mod_poly(r,mod);
	return r;
}

/*
	function that generates a radom number in an interval
*/
uint32_t rand_interval(uint32_t min, uint32_t max)
{
	int r;
	const uint32_t range = 1 + max - min;
	const uint32_t buckets = RAND_MAX / range;
	const uint32_t limit = buckets * range;

	do
	{
		r = rand();
	} while (r >= limit);

	return min + (r / buckets);
}

/*
	function useto display a polynomial
*/
void afficher_poly(poly *r)
{
	printf("R(x) = ");

	for (int i = 0; i <= r->degree; i++)
	{
		printf("%dx^%d +", r->degrees[i], i);
	}
}

/*
	function used to generate a random polynomial with degree n
	@params
	n: degree of the polynomial
*/
poly * generate_poly(int n){
	poly * p;


	p = malloc(sizeof(poly));
	initialize_to_zero(p, n);
	for (int i = 0; i <= n; i++)
	{
		p->degrees[i] = rand_interval(-100, 100);
	}

	return p;
}

/* function that can be used to store the values of the performances of each algo and then use the file to plot the results in a graph
*/

void ecrire_fichier(int degre, int choix, float time, char *file_name)
{

	char data[DATA_SIZE];
	FILE *fPtr;
	fPtr = fopen(file_name, "a+");
	if (fPtr == NULL)
	{
		printf("Unable to create file.\n");
		exit(EXIT_FAILURE);
	}
	char tmp[1000000] = "";
	if (choix == 1)
	{
		fputs("\n", fPtr);
		sprintf(tmp, "%d", degre);
		fputs(tmp, fPtr);
		fputs("_", fPtr);
		gcvt(time, 6, tmp);
		fputs(tmp, fPtr);
	}
	else
	{
		fputs("_", fPtr);
		gcvt(time, 6, tmp);
		fputs(tmp, fPtr);
	}
	fclose(fPtr);
}

/*this function serves to test the effeciency of each algo
	@params
	n: the maximum degree for which the function will test polynomial multiplication
	base: the basis in which the polynomials are being multiplied
*/

void test_function(int n , int base){
	clock_t t1,t2,t3, t_naive, t_karat, t_cook;
	poly *p;
	poly *q;
	poly *r1;
	poly *r2;
	poly *r3;
	for (int i = 1; i <= n; i++){
		t_karat = 0;
		t_naive = 0;
		printf("%d\n", i);
		for(int j  = 0; j<= 100; j++){
			p = generate_poly(i);
			q = generate_poly(i);


			t1= clock();
			r1 = naive_mod(p, q, base);
			t_naive += clock() - t1;
			free(r1);
			t2= clock();
			r2 = karatsuba_mod(i,p,q, base);
			t_karat += clock() - t2;
			free(r2);
			t3= clock();
			r3 = toom_cook3_mod(p,q, base);
			t_cook += clock() - t3;
			free(r3);
			free(p);
			free(q);
		}
		t_naive /=100;
		t_karat /=100;
		t_cook /=100;
		double t_n = ((double)t_naive) / CLOCKS_PER_SEC;
		printf("temps pris par naif :%f\n", t_n);
		double t_k = ((double)t_karat) / CLOCKS_PER_SEC;
		printf("temps pris par karatsuba:%f\n", t_k);
		double t_c = ((double)t_cook) / CLOCKS_PER_SEC;
		printf("temps pris par toom cook:%f\n", t_c);
		}
}

int main(int argc, char** argv){

	if(argc != 2){
		printf("Mauvaise utilisation de la commande main\n");
	}
	else{
		int base;
		if(!strcmp(argv[1],"simulate")){
			printf("MODE SIMULATION\n");
			printf("entrez la base sur laquelle vous voulez faire les tests:");
			scanf("%d",&base );
			clock_t t1,t2,t3, t_naive, t_karat, t_toom;
			int d1,d2,m;
			poly *p,*q,*r;
			printf("entrez le degrés du premier polynome sur lequel vous voulez effectué la simulation:");
			scanf("%d",&d1);
			m = d1;
			p =generate_poly(d1);
			printf("entrez le degrés du deuxieme polynome sur lequel vous voulez effectué la simulation:");
			scanf("%d",&d2);
			if(d2 >m){ m= d2;}
			q = generate_poly(d2);
			printf("Polynome P:\n");
			for( int i= 0; i<=p->degree; i++){
				printf("%dx^%d +",p->degrees[i],i);
			}
			printf("\n");
			printf("Polynome Q:\n");
			for( int i= 0; i<=q->degree; i++){
				printf("%dx^%d +",q->degrees[i],i);
			}
			printf("\n");
			initialize_to_zero(r, d1+d2);
			t1= clock();
			r = toom_cook3_mod(p,q,base);
			t_toom = clock() - t1;
			double t_t = ((double)t_toom) / CLOCKS_PER_SEC;
			printf("Tom cook: R(x) = ");
			for( int i= 0; i<=r->degree; i++){
				printf("%dx^%d +",r->degrees[i],i);
			}
			printf("temps pris par toom cook: %f seconde", t_t);
			printf("\n");
			printf("\n");
			/* KARATSUBA */
			printf("Karatsuba : R(x) = ");
			initialize_to_zero(r, d1+d2);
			t1= clock();
			r = karatsuba_mod(m,p,q,base);
			t_karat = clock() - t1;
			double t_k = ((double)t_karat) / CLOCKS_PER_SEC;
			for( int i= 0; i<=r->degree; i++){
				printf("%dx^%d +",r->degrees[i],i);
			}
			printf("temps pris par karatsuba: %f seconde", t_k);
			printf("\n");
			printf("\n");
			printf("Naive: R(x) = ");
			initialize_to_zero(r, d1+d2);
			t1= clock();
			r =naive_mod(p,q,base);
			t_naive = clock() - t1;
			double t_n = ((double)t_naive) / CLOCKS_PER_SEC;
			for( int i= 0; i<=r->degree; i++){
				printf("%dx^%d +",r->degrees[i],i);
			}
			printf("temps pris par naif: %f seconde", t_n);
			printf("\n********************************************\n");
			free(q);free(p);free(r);
			return 0;
		}
		else if(!strcmp(argv[1],"test")){
			printf("MODE TEST\n");
			printf("entrez la base sur laquelle vous voulez faire les tests:");
			scanf("%d",&base );
			int d_end;
			printf("entrez le degrés pour lequel vous voulez que les tests s'arretes:");
			scanf("%d",&d_end );
			test_function(d_end, base);
		}
		else if(!strcmp(argv[1],"compute")){
			printf("MODE CALCULE\n");
			printf("entrez la base sur laquelle vous voulez faire les tests:");
			scanf("%d",&base );
			clock_t t1,t2,t3, t_naive, t_karat, t_toom;
			int d1,d2,m, tmp;
			poly *p,*q,*r;
			p = malloc(sizeof(poly));
			q = malloc(sizeof(poly));
			r = malloc(sizeof(poly));
			printf("entrez le degrés du premier polynome sur lequel vous voulez effectué le calcule\n");
			scanf("%d",&d1);
			initialize_to_zero(p,d1);
			m = d1;
			for(int i = 0; i <= d1; i++){
				printf("entrez le coeff de degrés %d du premier polynome:",i );
				scanf("%d",&tmp);
				p->degrees[i] = tmp;
			}
			printf("entrez le degrés du deuxieme polynome sur lequel vous voulez effectué le calcule\n");
			scanf("%d",&d2);
			initialize_to_zero(q,d2);
			if(d2 >m){ m= d2;}
			for(int i = 0; i <= d2; i++){
				printf("entrez le coeff de degrés %d du premier polynome:",i );
				scanf("%d",&tmp);
				q->degrees[i] = tmp;
			}
			initialize_to_zero(r, d1+d2);
			t1= clock();
			r = toom_cook3_mod(p,q, base);
			t_toom = clock() - t1;
			double t_t = ((double)t_toom) / CLOCKS_PER_SEC;
			printf("Tom cook: R(x) = ");
			for( int i= 0; i<=r->degree; i++){
				printf("%dx^%d +",r->degrees[i],i);
			}
			printf("temps pris par toom cook: %f seconde", t_t);
			printf("\n");
			printf("\n");
			/* KARATSUBA */
			printf("Karatsuba : R(x) = ");
			t1= clock();
			r = karatsuba_mod(m,p,q, base);
			t_karat = clock() - t1;
			double t_k = ((double)t_karat) / CLOCKS_PER_SEC;
			for( int i= 0; i<=r->degree; i++){
				printf("%dx^%d +",r->degrees[i],i);
			}
			printf("temps pris par karatsuba: %f seconde", t_k);
			printf("\n");
			printf("\n");
			printf("Naive: R(x) = ");
			t1= clock();
			r =naive_mod(p,q, base);
			t_naive = clock() - t1;
			double t_n = ((double)t_naive) / CLOCKS_PER_SEC;
			for( int i= 0; i<=r->degree; i++){
				printf("%dx^%d +",r->degrees[i],i);
			}
			printf("temps pris par naif: %f seconde", t_n);
			printf("\n********************************************\n");
			return 0;
		}
		else{
			printf("argument invalide\n");
			return -1;
		}
		}
	}
